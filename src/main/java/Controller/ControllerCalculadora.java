package Controller;

import Model.Calculadora;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "ControllerCalculadora", urlPatterns = {"/ControllerCalculadora"})
public class ControllerCalculadora extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerCalculadora</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerCalculadora at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //processRequest(request, response);
        
        String c = request.getParameter("txtCapital");
        String ti = request.getParameter("txtTasaInteres");
        String a = request.getParameter("txtAgnos");
        String mensaje="";

        //Validar textos en blanco
        if (c.equals("")) mensaje = mensaje + "<br>" + "- Ingrese capital";
        if (ti.equals("")) mensaje = mensaje + "<br>" + "- Ingrese tasa de interés anual";
        if (a.equals("")) mensaje = mensaje + "<br>" + "- Ingrese años";
        if (!mensaje.equals(""))
        {
            request.setAttribute("error", (String)mensaje);
            request.getRequestDispatcher("error.jsp").forward(request, response);
        }
        
        int capital, tasaInteres, agnos;
        Calculadora cal = null; 
        capital = Integer.parseInt(c);
        tasaInteres = Integer.parseInt(ti);
        agnos = Integer.parseInt(a);
 
        //Validar rango de valores
        if (capital == 0) mensaje = mensaje + "<br>" + "- Capital debe ser mayor a 0";
        if (tasaInteres < 1 || tasaInteres > 100) mensaje = mensaje + "<br>" + "- Tasa de interés debe ser entre 1 y 100";
        if (agnos == 0) mensaje = mensaje + "<br>" + "- Años debe ser mayor a 0";
        if (!mensaje.equals(""))
        {
            request.setAttribute("error", (String)mensaje);
            request.getRequestDispatcher("error.jsp").forward(request, response);
        }
        else
        {
            cal = new Calculadora(capital, tasaInteres, agnos);
            request.setAttribute("calculadora", cal);
            request.getRequestDispatcher("resultado.jsp").forward(request, response);
        }
    }

    
    
    @Override
    public String getServletInfo() 
    {
        return "Short description";
    }

}
